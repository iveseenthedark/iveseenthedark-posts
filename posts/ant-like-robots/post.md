Robot-like Ants and Ant-like Robots
===================================

It'll not surpirse people who know me, that my first real blog-post is about Ants. From first dates, to drunk in the pub, my conversation has a habit of wandering over to Formicidae. What can I say - they're interesting.

This post duplicates the experiments in an article by _[Deneubourg et al.](https://www.researchgate.net/publication/235362107_The_dynamics_of_collective_sorting_robot-like_ants_and_ant-like_robots)_, which at it core uses two simple rules describle the behaviour of a colony of ant: 

1. Pick-up an item if you haven't seen one in a while,
2. Put-down anything you're carrying if you've seen a few similar items.

Intuatively, this results in a clustering behaviour in single brood environments, and sorting in a multi-brood evnironments. 

It's a neat and understandable example of self-organising behaviour. No single ant is activaly controlling the global behaviour, yet through local interactions with the environment, marco behaviours (sorting) occur.

The process of self-organisation where how complex functions can arise from simple mechanisims has always fasinated me, and can be applied to [numerous subjects](https://en.wikipedia.org/wiki/Self-organization#By_field).

Below is an implementation of the experiments. A colony of ants, moving randomly through these environment will pick-up and put-down brood using the functions : 

<iframe style="width:100%;border:none;height: 30rem;" src="https://iveseenthedark.bitbucket.io/ants/"></iframe>