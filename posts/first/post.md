A Terrible Idea for a Blog
==========================

I had an idea. It wasn't an **Idea**, but more one of those late night fancies that you drift into. Unlike so many of those twilight-twinklings, I decided that I would pursue this one. The idea was a simple one: What if I created a blog.

Hardly the most original of idea, nontheless, it's something that I think about every now and then, and so I decided that I would spend an evening building the most, basic, tralier-park version of a blog I could imagine. (Who needs Jykell and GitHub anyway!)

I was already using Bitbucket Cloud for a few of my other projects, so this seemed like an obvious choice to host my site. I also wanted some level of review before I pushed my humble (misspelled) words out into the world, so the opperturnity to send my editor girlfriend a PR for my latest post seemed a usuful feature to have.

The other ingredient for the folly is my current go-to for any quick, dirty projects: RectJS. I'm still fine-tuning my React chops, so I was excited by the idea of a new challange.

And thus the genesis of iveseenthepark blog. The code can be found [here](https://bitbucket.org/iveseenthedark/iveseenthedark-blog/src/develop/)



