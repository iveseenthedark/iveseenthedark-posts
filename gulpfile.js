const fs = require('fs');
const gulp = require('gulp');
const lqip = require('lqip');
const through = require('through2');
const fsutils = require('fs-utils');

const paths = { posts: './posts' };

/**
 */
const process_post_metadata = async (path, cb) => {
	const directory = fsutils.dirname(path);

	// Load/Create post metadata
	const metadata_path = `${directory}/metadata.json`;
	const metadata = fs.existsSync(metadata_path) ? fsutils.readJSONSync(metadata_path) : {};

	// Image name
	metadata.image = fsutils.last(path);

	// Low-Quality Image Placeholder
	const base64 = await lqip.base64(path);
	metadata.base64 = base64;
	console.log(`Wrote LQIP for image ${path}`);

	// Image Colour Palette
	const palette = await lqip.palette(path);
	metadata.palette = palette;
	console.log(`Wrote palette for image ${path}`);

	// Write metadata back to file
	fsutils.writeJSONSync(metadata_path, metadata);
	return cb();
};

/**
 */
gulp.task('lqip', () => {
	gulp.src(`${paths.posts}/**/*.jpg`).pipe(
		(dir => {
			return through.obj((file, enc, cb) => {
				if (file.isBuffer()) {
					return process_post_metadata(file.path, cb);
				}
				return cb(null, file); //no-op
			});
		})()
	);
});
